STACK_NAME=scamblock-pl
PARAMETERS_FILE=template-config.json
PARAMETERS=$(shell cat $(PARAMETERS_FILE) | jqn 'get("Parameters") | entries | map(x => x[0] + "=" + x[1]) | join(" ")')
REGION=eu-central-1
ARTIFACTS_BUCKET_NAME=artifacts.scamblock.pl
ARTIFACTS_S3_PREFIX=infra

ECR_REPO=776112957202.dkr.ecr.eu-central-1.amazonaws.com/scrapper
TEMPLATE_FILE=main.yml
CAPABILITIES=CAPABILITY_IAM CAPABILITY_AUTO_EXPAND
ARTIFACT_NAME=main.zip

build-scrapper:
	( cd ../scrapper && docker build -t $(ECR_REPO) . && docker push $(ECR_REPO) )

backend.zip:  $(shell ( cd ../backend && git ls-files "*.ts" | egrep ^src | xargs realpath ) )
	( cd ../backend && yarn build && node-lambda zip  -n backend -A . && mv backend.zip ../infrastructure/ )

upload-artifact: backend.zip
	sam package --output-template-file $(TEMPLATE_FILE) --s3-bucket $(ARTIFACTS_BUCKET_NAME) --s3-prefix $(ARTIFACTS_S3_PREFIX) --region $(REGION)
	zip $(ARTIFACT_NAME) $(TEMPLATE_FILE) $(PARAMETERS_FILE)
	aws s3 cp $(ARTIFACT_NAME) s3://$(ARTIFACTS_BUCKET_NAME)/$(ARTIFACTS_S3_PREFIX)/ --region $(REGION)

deploy: upload-artifact
	sam deploy --template-file $(TEMPLATE_FILE) --stack-name $(STACK_NAME) --capabilities $(CAPABILITIES)  --region $(REGION) --parameter-overrides $(PARAMETERS)


deploy-frontend:
	( cd ../frontend && \
	  yarn build && \
          aws s3 sync --exact-timestamps --delete build s3://scamblock.pl && \
	  aws cloudfront create-invalidation --distribution-id E1OVYMTHSGXYUM --paths "/*")
